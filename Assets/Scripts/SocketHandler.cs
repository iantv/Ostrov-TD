﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocketHandler : MonoBehaviour
{
    private TowerHandler Towers;
    public GameObject TowerLevel1;
    Animator TowerAnim;
    
    void Start ()
    {
        Towers = GameObject.FindGameObjectWithTag("Towers").GetComponent<TowerHandler>();
        TowerAnim = GameObject.FindGameObjectWithTag("BuildTowerPanel").GetComponent<Animator>();
	}
	
	void Update () {
		
	}

    private void OnMouseDown()
    {
        TowerAnim.SetTrigger("on");
        Towers.SetSocketPos(transform);
    }
}
